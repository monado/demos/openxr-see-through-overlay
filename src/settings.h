/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2017-2020 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <stdbool.h>

typedef struct
{
  char *device_path;
} settings;

bool
settings_parse_args(settings *self, int argc, char *argv[]);
