/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2021 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <stdbool.h>
#include <vulkan/vulkan.h>

#include "pipeline.h"

typedef struct _camera_gst camera_gst;
typedef struct _application application;

camera_gst *
camera_gst_new();

bool
camera_gst_init(camera_gst *self, const char *device_path);

void
camera_gst_set_pipeline(camera_gst *self, pipeline *pipe);

void
camera_gst_destroy(camera_gst *self);

void
camera_gst_set_first_frame_cb(camera_gst *self,
                              application *app,
                              void (*f)(application *a));
