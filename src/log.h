/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2017-2019 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#pragma once

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <stdbool.h>

#include "vulkan/vulkan.h"

#define log(...) log_full(__FILE__, __LINE__, __VA_ARGS__)
#define log_d(...) log(LOG_DEBUG, __VA_ARGS__)
#define log_i(...) log(LOG_INFO, __VA_ARGS__)
#define log_w(...) log(LOG_WARNING, __VA_ARGS__)
#define log_e(...) log(LOG_ERROR, __VA_ARGS__)
#define log_f(...) log(LOG_FATAL, __VA_ARGS__)
#define log_if(...) log_full_if(__FILE__, __LINE__, __VA_ARGS__)
#define log_f_if(...) log_if(LOG_FATAL, __VA_ARGS__)
#define log_e_if(...) log_if(LOG_ERROR, __VA_ARGS__)

// Macro to check and display Vulkan return results
#define vk_check(f)                                                            \
  {                                                                            \
    VkResult res = (f);                                                        \
    log_f_if(res != VK_SUCCESS, "VkResult: %s", vk_result_to_string(res));     \
  }

typedef enum
{
  LOG_DEBUG = 0,
  LOG_INFO,
  LOG_WARNING,
  LOG_ERROR,
  LOG_FATAL
} log_type;

const char *
vk_result_to_string(VkResult code);

const char *
log_type_str(log_type t);

int
log_type_color(log_type t);

FILE *
log_type_stream(log_type t);

void
log_values(
  const char *file, int line, log_type t, const char *format, va_list args);

void
log_full(const char *file, int line, log_type t, const char *format, ...);

void
log_full_if(
  const char *file, int line, log_type t, bool cond, const char *format, ...);
