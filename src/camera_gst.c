/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2021 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "camera_gst.h"

#include <stdbool.h>
#include <stdio.h>

#include <gst/gst.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <gst/vulkan/gstvkimagememory.h>
#pragma GCC diagnostic pop

#include "pipeline.h"
#include "log.h"

struct _camera_gst
{
  GObject parent;

  GstElement *playbin;

  gboolean playing;

  pipeline *pipe;

  VkImageView last_image_view;

  application *app;
  void (*first_frame_cb)(application *a);
};

camera_gst *
camera_gst_new()
{
  return malloc(sizeof(camera_gst));
}

void
camera_gst_set_pipeline(camera_gst *self, pipeline *pipe)
{
  self->pipe = pipe;
}

void
camera_gst_set_first_frame_cb(camera_gst *self,
                              application *app,
                              void (*f)(application *a))
{
  self->app = app;
  self->first_frame_cb = f;
}

void
camera_gst_destroy(camera_gst *self)
{
  gst_element_set_state(self->playbin, GST_STATE_NULL);
  gst_object_unref(self->playbin);
}

/* The appsink has received a buffer */
static GstFlowReturn
_sample_cb(GstElement *sink, camera_gst *self)
{
  GstSample *sample;

  /* Retrieve the buffer */
  g_signal_emit_by_name(sink, "pull-sample", &sample);
  if (sample) {
    GstBuffer *buffer = gst_sample_get_buffer(sample);
    GstMemory *mem = gst_buffer_peek_memory(buffer, 0);

    if (gst_is_vulkan_image_memory(mem)) {
      GstVulkanImageMemory *im = (GstVulkanImageMemory *)mem;
      VkImageViewCreateInfo image_view_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = im->image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = VK_FORMAT_R8G8B8A8_SRGB,
        .subresourceRange = {
          .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
          .baseMipLevel = 0,
          .levelCount = 1,
          .baseArrayLayer = 0,
          .layerCount = 1,
        },
      };

      VkDevice device = self->pipe->device;
      bool first_run = self->last_image_view == VK_NULL_HANDLE;

      if (first_run)
        self->first_frame_cb(self->app);
      else
        vkDestroyImageView(device, self->last_image_view, NULL);

      VkResult res = vkCreateImageView(device, &image_view_info, NULL,
                                       &self->last_image_view);
      if (res != VK_SUCCESS) {
        log_e("Could not create image view: %d", res);
        return GST_FLOW_ERROR;
      }

      pipeline_update_image_view(self->pipe, 0, self->last_image_view);
      pipeline_update_image_view(self->pipe, 1, self->last_image_view);
    }

    gst_sample_unref(sample);
    return GST_FLOW_OK;
  }

  return GST_FLOW_ERROR;
}

static void
_error_cb(GstBus *bus, GstMessage *msg, camera_gst *self)
{
  (void)bus;
  (void)self;

  GError *err;
  gchar *debug_info;

  /* Print error details on the screen */
  gst_message_parse_error(msg, &err, &debug_info);
  g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(msg->src),
             err->message);
  g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
  g_clear_error(&err);
  g_free(debug_info);
}

static void
_state_changed_cb(GstBus *bus, GstMessage *msg, camera_gst *self)
{
  (void)bus;
  GstState old_state, new_state, pending_state;
  gst_message_parse_state_changed(msg, &old_state, &new_state, &pending_state);

  if (GST_MESSAGE_SRC(msg) == GST_OBJECT(self->playbin)) {
    g_print("Pipeline state changed from %s to %s:\n",
            gst_element_state_get_name(old_state),
            gst_element_state_get_name(new_state));

    /* Remember whether we are in the PLAYING state or not */
    self->playing = (new_state == GST_STATE_PLAYING);

    if (new_state == GST_STATE_PAUSED) {
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(self->playbin),
                                GST_DEBUG_GRAPH_SHOW_ALL, "paused");
    } else if (new_state == GST_STATE_READY) {
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(self->playbin),
                                GST_DEBUG_GRAPH_SHOW_ALL, "ready");
    }

    if (self->playing) {
      GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(self->playbin),
                                GST_DEBUG_GRAPH_SHOW_ALL, "playing");
    }
  }
}

bool
camera_gst_init(camera_gst *self, const char *device_path)
{
  GstBus *bus;

  self->last_image_view = VK_NULL_HANDLE;

  GstElement *appsink, *vulkanupload, *capsfilter, *vulkan_app_sink, *v4l2src;

  /* Create the elements */
  self->playbin = gst_pipeline_new("pipeline");
  v4l2src = gst_element_factory_make("v4l2src", "v4l2src");
  vulkanupload = gst_element_factory_make("vulkanupload", "vulkanupload");
  appsink = gst_element_factory_make("appsink", "appsink");
  capsfilter = gst_element_factory_make("capsfilter", "capsfilter");

  GstElement *videoconvert =
    gst_element_factory_make("videoconvert", "videoconvert");

  /* Create the empty pipeline */
  if (!appsink) {
    g_printerr("Not all elements could be created.\n");
    return false;
  }

  log_i("Using device_path %s", device_path);
  g_object_set(v4l2src, "device", device_path, NULL);

  GstCaps *vkimagecaps =
    gst_caps_new_simple("video/x-raw", "format", G_TYPE_STRING, "RGBA", NULL);

  GstCapsFeatures *vkimagefeatures =
    gst_caps_features_new("memory:VulkanImage", NULL);

  gst_caps_set_features(vkimagecaps, 0, vkimagefeatures);
  g_object_set(capsfilter, "caps", vkimagecaps, NULL);

  vulkan_app_sink = gst_bin_new("vulkanappsink");

  gst_bin_add_many(GST_BIN(vulkan_app_sink), videoconvert, vulkanupload,
                   capsfilter, appsink, NULL);
  gst_element_link_many(videoconvert, vulkanupload, capsfilter, appsink, NULL);

  GstPad *pad = gst_element_get_static_pad(videoconvert, "sink");
  GstPad *ghost_pad = gst_ghost_pad_new("sink", pad);
  gst_pad_set_active(ghost_pad, TRUE);
  gst_element_add_pad(vulkan_app_sink, ghost_pad);
  gst_object_unref(pad);


  gst_bin_add_many(GST_BIN(self->playbin), v4l2src, vulkan_app_sink, NULL);
  gst_element_link_many(v4l2src, vulkan_app_sink, NULL);

  /* Configure appsink */
  g_object_set(appsink, "emit-signals", TRUE, NULL);
  g_signal_connect(appsink, "new-sample", G_CALLBACK(_sample_cb), self);

  /* Instruct the bus to emit signals for each received message, and connect to
   * the interesting signals */
  bus = gst_element_get_bus(self->playbin);
  gst_bus_add_signal_watch(bus);
  g_signal_connect(G_OBJECT(bus), "message::error", G_CALLBACK(_error_cb),
                   self);
  g_signal_connect(G_OBJECT(bus), "message::state-changed",
                   G_CALLBACK(_state_changed_cb), self);
  gst_object_unref(bus);

  /* Start playing the pipeline */
  gst_element_set_state(self->playbin, GST_STATE_PLAYING);

  return true;
}
