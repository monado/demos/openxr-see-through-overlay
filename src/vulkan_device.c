/*
 * OpenXR Video Seethrough Overlay
 *
 * Copyright 2016 Sascha Willems - www.saschawillems.de
 * Copyright 2017-2020 Collabora Ltd.
 *
 * Authors: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "vulkan_device.h"

static bool
_get_graphics_queue_index(vulkan_device *self)
{
  for (uint32_t i = 0; i < self->queue_family_count; i++) {
    if (self->queue_family_properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
      self->graphics_family_index = i;
      return true;
    }
  }

  self->graphics_family_index = 0;
  return false;
}

vulkan_device *
vulkan_device_create(VkPhysicalDevice physical_device)
{
  vulkan_device *self = malloc(sizeof(vulkan_device));

  assert(physical_device);
  self->physical_device = physical_device;

  vkGetPhysicalDeviceProperties(physical_device, &self->properties);
  vkGetPhysicalDeviceFeatures(physical_device, &self->features);
  vkGetPhysicalDeviceMemoryProperties(physical_device,
                                      &self->memory_properties);

  vkGetPhysicalDeviceQueueFamilyProperties(physical_device,
                                           &self->queue_family_count, NULL);
  assert(self->queue_family_count > 0);
  self->queue_family_properties =
    malloc(sizeof(VkQueueFamilyProperties) * self->queue_family_count);
  vkGetPhysicalDeviceQueueFamilyProperties(
    physical_device, &self->queue_family_count, self->queue_family_properties);

  if (!_get_graphics_queue_index(self))
    log_e("Could not find graphics queue.");

  self->cmd_pool = NULL;

  return self;
}

void
vulkan_device_destroy(vulkan_device *self)
{
  if (self->cmd_pool)
    vkDestroyCommandPool(self->device, self->cmd_pool, NULL);
  if (self->device)
    vkDestroyDevice(self->device, NULL);
  free(self);
}

bool
vulkan_device_get_memory_type(vulkan_device *self,
                              uint32_t typeBits,
                              VkMemoryPropertyFlags properties,
                              uint32_t *out_index)
{
  for (uint32_t i = 0; i < self->memory_properties.memoryTypeCount; i++) {
    if ((typeBits & 1) == 1) {
      if ((self->memory_properties.memoryTypes[i].propertyFlags & properties) ==
          properties) {
        *out_index = i;
        return true;
      }
    }
    typeBits >>= 1;
  }

  *out_index = 0;
  return false;
}

static VkResult
_create_buffer(vulkan_device *self,
               vulkan_buffer *buffer,
               VkBufferUsageFlags usage,
               VkMemoryPropertyFlags memory_flags,
               VkDeviceSize size,
               void *data)
{
  buffer->device = self->device;

  // Create the buffer handle
  VkBufferCreateInfo bufferCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
    .size = size,
    .usage = usage,
  };
  vk_check(
    vkCreateBuffer(self->device, &bufferCreateInfo, NULL, &buffer->buffer));

  // Create the memory backing up the buffer handle
  VkMemoryRequirements memReqs;
  vkGetBufferMemoryRequirements(self->device, buffer->buffer, &memReqs);

  VkMemoryAllocateInfo memAlloc = {
    .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    .allocationSize = memReqs.size,
  };

  // Find a memory type index that fits the properties of the buffer
  if (!vulkan_device_get_memory_type(self, memReqs.memoryTypeBits, memory_flags,
                                     &memAlloc.memoryTypeIndex))
    log_e("Could not find memory type.");

  vk_check(vkAllocateMemory(self->device, &memAlloc, NULL, &buffer->memory));

  buffer->alignment = memReqs.alignment;
  buffer->size = memAlloc.allocationSize;
  buffer->usage_flags = usage;
  buffer->memory_property_flags = memory_flags;

  // If a pointer to the buffer data has been passed, map the buffer and copy
  // over the data
  if (data != NULL) {
    vk_check(vulkan_buffer_map(buffer));
    memcpy(buffer->mapped, data, size);
    vulkan_buffer_unmap(buffer);
  }

  // Initialize a default descriptor that covers the whole buffer size
  vulkan_buffer_setup_descriptor(buffer);

  // Attach the memory to the buffer object
  return vulkan_buffer_bind(buffer);
}

void
vulkan_device_create_and_map(vulkan_device *self,
                             vulkan_buffer *buffer,
                             VkDeviceSize size)
{
  VkMemoryPropertyFlags memory_flags =
    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
  vk_check(_create_buffer(self, buffer, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                          memory_flags, size, NULL));

  // Map persistent
  vk_check(vulkan_buffer_map(buffer));
}


